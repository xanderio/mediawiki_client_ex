This file is meant to split future development into phases.

## 1.x-TODO

_Features which are out of scope for the moment.  Won't start working on these until the 0.x series is complete._

### To add

- Discover APIs (and warn that this should be configured?  cache?), generate matching client.
- Wikimedia Commons media commands.
- Clients for some of the many other Wikimedia [REST API](https://www.mediawiki.org/wiki/REST_API )s beyond core, served through RESTBase. See [issue #2](https://gitlab.com/adamwight/mediawiki_client_ex/-/issues/2).
- Demonstrate a cross-wiki API call (CentralAuth).
- Instrument how many calls are made, when, and bandwidth used.
- Wikidata Query Service

## 0.99-TODO

_Stable release milestone: Features which should be implemented in order to finish the 0.x series._

### To add

- Detect server and network errors, fail fast.  Show helpful API debugging in dev environment.  Demonstrate how to call with error handling.
- Longer, configurable default timeouts to match servers. (#18)
- Convenient logging—a global setting to inject the logging middleware into all client plugs.
- ...

## 0.5-TODO

_Current development target_

### To add

- Show how to integrate with the MediaWiki OAuth2 provider (to be published), to authenticate actions on behalf of a user.
- Built-in Mediawiki [REST API](https://www.mediawiki.org/wiki/API:REST_API)

### To change

- Cache SiteMatrix globally. (#12)
