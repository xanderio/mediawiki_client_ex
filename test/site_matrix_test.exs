defmodule SiteTest do
  use ExUnit.Case, async: true

  alias Tesla.Env
  alias Wiki.SiteMatrix
  alias Wiki.Tests.TeslaAdapterMock

  import Mox

  setup :verify_on_exit!

  @response ~S(
    {
      "sitematrix": {
        "0": {
          "code": "aa",
          "name": "Qafár af",
          "site": [
            {
              "url": "https://aa.wikipedia.org",
              "dbname": "aawiki",
              "code": "wiki",
              "lang": "aa",
              "sitename": "Wikipedia",
              "closed": true
            },
            {
              "url": "https://aa.wiktionary.org",
              "dbname": "aawiktionary",
              "code": "wiktionary",
              "lang": "aa",
              "sitename": "Wiktionary",
              "closed": true
            }
          ],
          "dir": "rtl",
          "localname": "Afar"
        },
        "1": {
          "code": "ak",
          "dir": "ltr",
          "name": null,
          "site": [
            {
              "closed": true,
              "code": "wiki",
              "dbname": "akwiki",
              "lang": "ak",
              "sitename": "Wikipedia",
              "url": "https://ak.wikipedia.org"
            }
          ]
        },
        "count": 4,
        "specials": [
          {
            "url": "https://advisors.wikimedia.org",
            "dbname": "advisorswiki",
            "code": "advisors",
            "lang": "advisors",
            "sitename": "Advisors",
            "private": true
          },
          {
            "url": "https://advisory.wikimedia.org",
            "dbname": "advisorywiki",
            "code": "advisory",
            "lang": "en",
            "sitename": "Advisory Board",
            "closed": true
          }
        ]
      }
    }
  ) |> Jason.decode!()

  defp create_sitematrix do
    SiteMatrix.new(adapter: TeslaAdapterMock)
  end

  test "get_all sites" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      headers = [{"content-type", "application/json; charset=utf-8"}]
      {:ok, %Env{env | body: @response, headers: headers, status: 200}}
    end)

    assert create_sitematrix() |> SiteMatrix.get_all() ==
             {:ok,
              [
                %SiteMatrix.Spec{
                  base_url: "https://aa.wikipedia.org",
                  closed: true,
                  dbname: "aawiki",
                  dir: "rtl",
                  lang: "aa",
                  name: "Afar Wikipedia",
                  private: false,
                  project: "wiki"
                },
                %SiteMatrix.Spec{
                  base_url: "https://aa.wiktionary.org",
                  closed: true,
                  dbname: "aawiktionary",
                  dir: "rtl",
                  lang: "aa",
                  name: "Afar Wiktionary",
                  private: false,
                  project: "wiktionary"
                },
                %SiteMatrix.Spec{
                  base_url: "https://advisors.wikimedia.org",
                  closed: false,
                  dbname: "advisorswiki",
                  dir: "ltr",
                  lang: "advisors",
                  name: "Advisors",
                  private: true,
                  project: "advisors"
                },
                %SiteMatrix.Spec{
                  base_url: "https://advisory.wikimedia.org",
                  closed: true,
                  dbname: "advisorywiki",
                  dir: "ltr",
                  lang: "en",
                  name: "Advisory Board",
                  private: false,
                  project: "advisory"
                },
                %SiteMatrix.Spec{
                  base_url: "https://ak.wikipedia.org",
                  closed: true,
                  dbname: "akwiki",
                  dir: "ltr",
                  lang: "ak",
                  name: "ak Wikipedia",
                  private: false,
                  project: "wiki"
                }
              ]}
  end

  test "get one site" do
    TeslaAdapterMock
    |> expect(:call, 1, fn env, _opts ->
      headers = [{"content-type", "application/json; charset=utf-8"}]
      {:ok, %Env{env | body: @response, headers: headers, status: 200}}
    end)

    sitematrix = create_sitematrix()

    assert sitematrix |> SiteMatrix.get!("aawiktionary") == %SiteMatrix.Spec{
             base_url: "https://aa.wiktionary.org",
             closed: true,
             dbname: "aawiktionary",
             dir: "rtl",
             lang: "aa",
             name: "Afar Wiktionary",
             private: false,
             project: "wiktionary"
           }

    assert sitematrix |> SiteMatrix.get("zzwiktionary") ==
             {:error, %Wiki.Error{message: "Site zzwiktionary not found."}}
  end
end
